import QtQuick 2.9
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import QtQuick.Window 2.2
import Morph.Web 0.1
import "UCSComponents"
import QtWebEngine 1.7
import QtSystemInfo 5.5
import "actions" as Actions

MainView {
id:window

ScreenSaver {
id: screenSaver
screenSaverEnabled: !(Qt.application.active)
}
    objectName: "mainView"

    applicationName: "jubilee.joe"

    property string myUrl: "../www/a.htm"

backgroundColor : "transparent"

            function navigationRequestedDelegate(request) {
                var url = request.url.toString();

                if(isValid(url) == false) {
                    console.warn("Opening remote: " + url);
                    Qt.openUrlExternally(url)
                    request.action = WebEngineNavigationRequest.IgnoreRequest;
                }
            }    

        
   WebEngineView {
   id: webview
    anchors{ fill: parent}


 
              settings.fullScreenSupportEnabled: true
       property var currentWebview: webview
           settings.pluginsEnabled: true

           
                    onFullScreenRequested: function(request) {
       nav.visible = !nav.visible

       request.accept();
   }

// Do later
//onContextMenuRequested: function(request) {
//        request.accepted = true;
//        contextMenu.x = request.x;
//        contextMenu.y = request.y;
//        contextMenu.popup();
//    }
       
profile:  WebEngineProfile{
id: webContext
    persistentCookiesPolicy: WebEngineProfile.ForcePersistentCookies
       property alias dataPath: webContext.persistentStoragePath

            dataPath: dataLocation


    
        httpUserAgent: "Mozilla/5.0 (Linux; Android 8.0.0; Pixel Build/OPR3.170623.007) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.98 Mobile Safari/537.36"
    }



              

       url: myUrl
 userScripts: [
WebEngineScript {
  injectionPoint: WebEngineScript.DocumentCreation
worldId: WebEngineScript.MainWorld
name: "QWebChannel"
sourceUrl: "ubuntutheme.js"
}
]

    
    }
       RadialBottomEdge {
            id: nav
            visible: true
            actions: [
                RadialAction {
                    id: home
                    iconName: "home"
                    onTriggered: {
                        webview.url = myUrl
                    }
                    text: qsTr("Home")
                },

                RadialAction {
                    id: forward
                    enabled: webview.canGoForward
                    iconName: "go-next"
                    onTriggered: {
                        webview.goForward()
                    }
                   text: qsTr("Forward")
                 },
                  RadialAction {
                    id: search
                    iconName: "search"
                    onTriggered: {
                        webview.url = '../www/vsearch.htm'
                    }
                    text: qsTr("Search")
                },
                 RadialAction {
                    id: list
                    iconName: "edit"
                    onTriggered: {
                        webview.url = '../www/listv.htm'
                    }
                    text: qsTr("List Verses")
                },
                              
                  RadialAction {
                    id: back
                    enabled: webview.canGoBack
                    iconName: "go-previous"
                    onTriggered: {
                        webview.goBack()
                    }
                    text: qsTr("Back")
                }
                
            ]
        }
}
