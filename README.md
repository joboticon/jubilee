## Jubilee Bible

The Jubilee has come!
Jubilee Bible app ported to Ubuntu Touch:
English Bible with outlines and study notes. 

## How to build

Set up [clickable](https://github.com/bhdouglass/clickable) and run `clickable` inside the working directory.

# Known Issues / Help Wanted:

* No copy and paste (qtwebengine)

